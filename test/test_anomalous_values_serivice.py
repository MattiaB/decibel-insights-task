import json
import os

import pytest

from app import create_app


@pytest.fixture(scope='module')
def test_client():
    os.environ['APP_SETTINGS'] = "config.TestingConfig"
    flask_app = create_app('config/flask_test.cfg')

    # Flask provides a way to test your application
    # by exposing the Werkzeug test Client
    # and handling the context locals for you.
    testing_client = flask_app.test_client()

    # Establish an application context before running the tests.
    ctx = flask_app.app_context()
    ctx.push()

    yield testing_client  # this is where the testing happens!

    ctx.pop()


def test_less_than_50_values(test_client):
    """
    GIVEN a Flask application
    WHEN the '/get_anomalous_data' page is requested (POST)
    THEN check the response is valid
    """
    response = test_client.post('/get_anomalous_data',
                                data=json.dumps([[42, 42]]),
                                content_type='application/json')
    assert response.status_code == 202
