FROM python:3.7
MAINTAINER mattia.bertorello@gmail.com

# Copy the Pipfile and Pipfile.lock
COPY Pipfile* /app/

WORKDIR /app
# Install the service dependencies
RUN export  \
    && pip install --no-cache-dir pipenv \
    # https://stackoverflow.com/a/49705601
    && pipenv install --system --deploy --ignore-pipfile
ADD app /app
# Configure the default flask configuration file
ENV FLASK_CONFIG=flask_prod.cfg

EXPOSE 8000
# Run gunicorn on the 8000 port
CMD ["gunicorn", "-b", "0.0.0.0:8000", "wsgi:app"]