import numpy as np
from scipy.spatial.distance import mahalanobis


def nd_rolling(data, window_size):
    """
    Generate the moving window

    data: pandas.core.frame.DataFrame
    window_size: float

    yield: tuple
    """

    # calculate the moving window for each idx-th point
    for idx, row in data.iterrows():
        idx0 = idx if idx - window_size < 0 else idx - window_size
        idx0_end = idx0 + window_size
        # Extract the window
        window = data.iloc[idx0:idx0_end]

        window_mean = window.mean(axis=0)
        try:
            window_cov_inv = np.linalg.inv(window.cov())
            yield {'idx': idx,
                   'value': np.array(row),
                   'window_avg': window_mean,
                   'window_cov_inv': window_cov_inv
                   }
        except np.linalg.LinAlgError:
            # https://it.mathworks.com/matlabcentral/answers/333008-to-calculate-mahalanobis-distance-when-the-number-of-observations-are-less-than-the-dimension
            # We don't know there is no information in the data
            pass


def distance(p):
    return mahalanobis(p['value'], p['window_avg'],
                       p['window_cov_inv'])


def get_anomalous_values(data, window_size, prob=0.99):
    """
    return a list of anomalous values, i.e. the ones that exceed md times
    in terms of Mohalanobis distance the expected multivariate average. Both
    multivariate average and Mahalanobis distance are calculated considering
    the moving windows, i.e. the value computed considering window_size
    neighbours, moving the window for each value of the serie.

    data : pandas.core.frame.DataFrame
    window_size: int
    md: float

    return: list
    """

    # under normal hypotesis, the Mohalanobis dinstance is Chi-squared
    # distribuited
    threshold = np.sqrt(-2 * np.log(1 - prob))

    # calculate the moving window for each point, and report the anomaly if
    # the distance of the idx-th point is greater than md times the mahalanobis
    # distance
    # https://stats.stackexchange.com/questions/37743/singular-covariance-matrix-in-mahalanobis-distance-in-matlab
    return [(p['idx'], p['value'].tolist())
            for p in nd_rolling(data, window_size) if distance(p) > threshold]
