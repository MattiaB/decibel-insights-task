import os


class Config(object):
    REQUEST_ID_UNIQUE_VALUE_PREFIX = 'decibel-'
    WINDOWS_SIZE = int(os.getenv('WINDOWS_SIZE', 500))
    CHI_SQUARED_PROBABILITY = float(os.getenv('CHI_SQUARED_PROBABILITY', 0.99))
