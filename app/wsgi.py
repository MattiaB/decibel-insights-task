import os

import logging_config
from app import create_app

# https://stackoverflow.com/questions/25319690/how-do-i-run-a-flask-app-in-gunicorn-if-i-used-the-application-factory-pattern
app = create_app(f"config/{os.getenv('FLASK_CONFIG') or 'flask_dev.cfg'}")

if __name__ == '__main__':
    logging_config.setup()
    app.run(host='127.0.0.1')
