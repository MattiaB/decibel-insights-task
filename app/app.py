from flask import Flask
from flask_json_schema import JsonSchema
from flask_request_id_header.middleware import RequestID

import config
import logging_config

schema = JsonSchema()


def create_app(config_filename):
    """
    Configure the flask application

    :param config_filename: the path of the flash configuration file
    :return:
    """
    app = Flask(__name__)
    schema.init_app(app)
    # Add the encoded configuration
    app.config.from_object(config.Config)
    app.config.from_pyfile(config_filename)

    import anomaly_detection_api
    import errors
    # Initialize the error gathering
    app.register_blueprint(errors.blueprint)
    # Initialize the actual anomaly detection api
    app.register_blueprint(anomaly_detection_api.api)

    # Add the request id to the flask application
    # Will be easy to track error in a complex microservices environment
    RequestID(app)
    logging_config.setup()

    return app
