import pandas as pd
import structlog
from flask import current_app
from flask import request, jsonify, Blueprint

from anomalous_values_service import get_anomalous_values
from app import schema

api = Blueprint(name='api', import_name=__name__)

# json schema validation for the get_anomalous_data api
get_anomalous_data_schema = {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "type": "array",
    "items": {
        "type": "array",
        "items": {
            "type": "number"
        },
        "maxItems": 2,
        "minItems": 2
    }
}
# tuple index to get the index of the anomalous value
IDX_ANOMALOUS_VALUE_TUPLE_INDEX: int = 0
# tuple index to get the anomalous value
ANOMALOUS_VALUE_TUPLE_INDEX: int = 1

log = structlog.getLogger(__name__)


@api.route('/get_anomalous_data', methods=['POST'])
@schema.validate(get_anomalous_data_schema)
def get_anomalous_data():
    """
    From a list of value it return a list of anomalous values
    find by the algorithm see
    :func:`~anomalous_values_service.get_anomalous_values`

    curl -X POST -H "Content-Type: application/json" \
            -d '[[1.23,1.24],[1.345,2.345],[1.229,1.241]]' \
            http://localhost:8080/get_anomalous_data
    :return: An array of object of anomalous values
    """
    object_request_body = request.get_json()

    # columns
    columns = ['X', 'Y']
    # Convert the list of points in a dataFrame
    data = pd.DataFrame(object_request_body, columns=columns)
    log.info(f'Receive {len(data)} elements to calculate the anomalous')

    # If the len of the dataFrame is less than 50 means
    # that there aren't enough
    # to return a meaningful result of anomalous values
    if len(data) <= 50:
        log.debug(f'Receive {len(data)} elements are not enough, '
                  f'the array must contain at least 50 elements ')
        return jsonify([]), 202

    # Run the algorithm to find the anomalous values
    anomalous_values = \
        get_anomalous_values(data=data,
                             window_size=current_app.config['WINDOWS_SIZE'],
                             prob=current_app.config['CHI_SQUARED_PROBABILITY']
                             )
    # Convert the result example:
    # [{'2': [1.345, 2.345]}]
    log.info(f"Total anomalous values found {len(anomalous_values)} "
             f"in this data set size {len(data)}")
    return jsonify([
        {v[IDX_ANOMALOUS_VALUE_TUPLE_INDEX]: v[ANOMALOUS_VALUE_TUPLE_INDEX:]}
        for v in anomalous_values
    ])


@api.route('/health', methods=['GET'])
def health():
    """
    Used by the health check to determinate if the service is up or not

    :return: Always "ok" as a body and 200 as a status code
    """
    return "ok", 200
