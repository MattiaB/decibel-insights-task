"""Application error handlers."""
import structlog
from flask import Blueprint, jsonify, request
from flask_json_schema import JsonValidationError

blueprint = Blueprint(name='errors', import_name=__name__)

log = structlog.getLogger(__name__)


@blueprint.app_errorhandler(Exception)
def handle_unexpected_error(error):
    log.exception("An unexpected error has occurred")
    status_code = 500
    success = False
    response = {
        'success': success,
        'error': {
            'type': 'UnexpectedException',
            'message': 'An unexpected error has occurred.'
        },
        'request_id': request.environ.get("HTTP_X_REQUEST_ID"),
    }

    return jsonify(response), status_code


@blueprint.errorhandler(JsonValidationError)
def validation_error(e):
    return jsonify({
        'error': e.message,
        'errors': [validation_err.message for validation_err in e.errors],
        'request_id': request.environ.get("HTTP_X_REQUEST_ID"),
    })
