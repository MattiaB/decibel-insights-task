# Decibel Insights Task

## The anomaly detector algorithm 
A high-level overview of the algorithm takes an array of 2 values that we called x,y and It recognises anomalous values in this array.
It calculates the threshold given a probability under normal hypothesis.
- For every value of the dataset (size N) 
  - It takes a sample of size M
  - It calculates the mean and the covariance of the sample
  - Then will extract the Mahalanobis distance between every value and the average of the sample with the inverse of the covariance matrix of the sample.

If the dataset is made up of points with a Gaussian distribution, 
It possible to use the [Mahalanobis](https://en.wikipedia.org/wiki/Mahalanobis_distance#Normal_distributions) to calculate the dissimilarity 
between a single value and the mean of the sample (in this particular case).


The algorithm uses the concept Sliding Window to calculate only to a subset of data (the sample) the mean and the covariance. 
So at every new value, it moves the windows of one element ahead and removes the previous one. 
The windows size can be configured.

### Libraries 
The libraries used are pandas to read the data, scipy to calculate the Mahalanobis distance, numpy to calculate the mean and the covariance matrix

### Issue found 
This code will generate always a window with the size of the sample because `it[1]` is the x value not the index
```python
window = [it for it in sample
          if it[0] >= idx0 or it[1] <= idx0 + window_size]
```
Fix: 
```python
 window = [it for it in sample
            if (idx0 <= it[0] < idx0 + window_size) 
                  or window_size >= len(sample)] # check also when the windows size is bigger than the sample
```
-----
 When the covariance matrix is as singular matrix, 
 the Mahalanobis as no meaning because there are no enough information. 
 [Source](https://it.mathworks.com/matlabcentral/answers/333008-to-calculate-mahalanobis-distance-when-the-number-of-observations-are-less-than-the-dimension),
 [Wiki](https://en.wikipedia.org/wiki/Mahalanobis_distance)
```python
from scipy.spatial.distance import mahalanobis, euclidean
import numpy as np
try:
    window_cov_inv = np.linalg.inv(window.cov())
    yield {'idx': idx,
           'value': np.array(row),
           'window_avg': window_mean,
           'window_cov_inv': window_cov_inv
           }
except np.linalg.LinAlgError:
    # https://it.mathworks.com/matlabcentral/answers/333008-to-calculate-mahalanobis-distance-when-the-number-of-observations-are-less-than-the-dimension
    # We don't know there is no information in the data
    pass
```

### Optimizations
The mix different python function like the `zip` function, list comprehension, numpy array, tuple.
So to make more efficient and uniform it can be transform all code in pandas functions.

This code replicates the behaviour of the prototype one but uses almost only pandas methods.
```python
import numpy as np
for idx, row in data.iterrows():
    idx0 = idx if idx - window_size < 0 else idx - window_size
    idx0_end = idx0 + window_size
    window = data.iloc[idx0:idx0_end]
    
    window_mean = window.mean(axis = 0) 
    window_cov = window.cov()

    yield {'idx': idx,
           'value': np.array(row),
           'window_avg': window_mean,
           'window_cov': window_cov}
```  


Another optimization can be the possibility to get rid of the sliding window and use a fixed size of the windows. 
This could change a little bit the results, I need to investigate more. 
```python
import numpy as np
for idx, row in data.iterrows():
    if idx % window_size == 0:
        idx0 = idx if idx - window_size < 0 else idx - window_size
        idx0_end = idx0 + window_size
        window = data.iloc[idx0:idx0_end]
        
        window_mean = window.mean(axis = 0) 
        window_cov = window.cov()

    yield {'idx': idx,
           'value': np.array(row),
           'window_avg': window_mean,
           'window_cov': window_cov}
```  

These are some results of the optimizations of the algorithm.
```
original:	data size=5002 time=7.83s,	anomalous_values size= 15
optimization 1:	data size=5002 time=5.28s,	anomalous_values size= 15
optimization 2:	data size=5002 time=0.57s,	anomalous_values size= 1
...
original:	data size=10002 time=28.99s,	anomalous_values size= 45
optimization 1:	data size=10002 time=9.31s,	anomalous_values size= 46
optimization 2:	data size=10002 time=1.04s,	anomalous_values size= 1
```
[![Performance analysis](doc/img/perfomance_analysis.png)]()

We see that at the end the different is pretty large between the three optimization 

### TODO
- Improve the performance test of the algorithm 
- Check with the data science if the replacement of the sliding windows doesn't change the results of the algorithm.
  It changes the results but could be not statistical relevance.

## Design the pipeline
To built the microservice I use the library Flask that let me write few lines of code to expose the HTTP endpoint. 
Moreover, the library is very active in the Python community so it easy to find all the stuff you need to manage and monitor the service.

### Errors
If something goes wrong the service will return a JSON error with the request-id that can be useful to search it in the logs

### Input
The JSON of the body request is validated with JSON schema by [flask-json-schema](https://pypi.org/project/flask-json-schema/)
to avoid bad input from the client. 
That could generate strange errors difficult to debug. 

### Testing 
I choose [pytest](https://docs.pytest.org/en/latest/) to test the endpoint, 
due to his support by the community and useful features 

### Configuration 

The service could be configured with a configuration file. 
The path of the file must be put in an environment variable `FLASK_CONFIG`

Example: `export FLASK_CONFIG=flask_dev.cfg`

### Run the WSGI
You can use flask cli to run the wsgi: 
`LOG_LEVEL=DEBUG FLASK_ENV=development pipenv run flask run`
This command will start a HTTP on the 5000 port
with the logging set to debug and the auto restart when some code change.

### DevOps stuff 
- To build and run the docker image
  ```bash
  docker build -t anomaly-detection-api:latest .
  docker run --rm  -e FLASK_CONFIG=flask_dev.cfg -e LOG_LEVEL=DEBUG  \
    -p 8000:8000 anomaly-detection-api:latest
  ```
-  I put a code style checker to let code with a standard format [Link](https://www.python.org/dev/peps/)
-  Support the request-id in the headers and the logging to filter better the request in the logging platform 
   and also correlating request across microservices.
- With JSON logging is easier to put custom field and search in through the log.
- Use the Bitbucket pipeline to run continuous integration task and also the possibility to deploy in production.
  The continuous integration gives to the developer fast feedback if he writes something that is not compliant with the code style or not passes the test. 
  These tools try to maintain the code base uniform and high quality 


### TODO
- Expose a swagger YAML to document the API
- Increase the test coverage
- Use the [Prometheus client](https://github.com/prometheus/client_python) to expose custom metrics of the service to enhance the monitoring.
- Use [OpenTracing](https://github.com/opentracing/opentracing-python) to profile and monitor the application. 
  It helps pinpoint where failures occur and what causes poor performance. [Source](https://opentracing.io/docs/overview/what-is-tracing/)

## Deploy the pipeline

The infrastructure to deploy the pipeline is composed of AWS CloudFormation, Ansible, Vagrant.
I start with Ansible, his job is to configure the instance with the docker service and run the API container.

To install the docker service I use two ansible role done by geerlingguy: 
[pip](https://github.com/geerlingguy/ansible-role-pip), [docker](https://github.com/geerlingguy/ansible-role-docker).
Than I use the docker_container module to start the container. In this case the docker image is public but can be hosted 
in a private repository.
```yaml
docker_container:
    name: anomaly-detection-api-modest-galileo
    image: mattiabertorello/anomaly-detection-api:latest
    # Check if there are new version due to the use the tag latest
    pull: true
    state: started
    restart_policy: unless-stopped # If it crash for some reason the API will be restart
    env:
      # Set the prod configuration
      FLASK_CONFIG: "flask_prod.cfg"
    published_ports:
      - 80:8000 # The API will be reachable on the 80 port, inside the container It must listen on the 8000 port
```
### How I built it
I test the playbook manually with Vangrant by provision the VM with a Vagrant file. 
This allows anyone to run the playbook without launch an instance on AWS. 
The next step could be to introduce automated testing also for the playbook. 

### Run in local
In the folder `_infra`
- Basic requirements: [Python](https://realpython.com/installing-python/), 
[pip](https://pip.pypa.io/en/stable/installing/), [Vagrant](https://www.vagrantup.com/intro/getting-started/install.html)
- Run these commands
```bash
pip install pipenv
pipenv install      # to install Ansible and awscli
pipenv run ansible-galaxy install -r requirements.yml  # to install the Ansible roles
vagrant up          # to run and provisioning the VM
vagrant ssh         # to connect inside the machine and test the service 
curl -X POST  -v -H "Content-Type: application/json" -d @../test/testdata/data_1.json \
http://192.168.33.39/get_anomalous_data
```

### Deploy on AWS:
In the folder `_infra`
- Basic requirements: [Python](https://realpython.com/installing-python/), 
[pip](https://pip.pypa.io/en/stable/installing/), AWS Account, OpenSSH
- Generate a private key `ssh-keygen -t rsa -b 2048 -C "decibel-task" -f id_rsa_decibel`
- Remember to import it in the AWS console [Link](https://console.aws.amazon.com/ec2/home?region=us-east-1#KeyPairs)
- Generate an AWS Key [Link](https://console.aws.amazon.com/iam/home#/users) with policies AmazonEC2FullAccess, AWSCloudFormationFullAccess
- Export in the local environment the AWS Key 
```bash
export AWS_ACCESS_KEY_ID=replace_with_aws_access_key_id
export AWS_SECRET_ACCESS_KEY=replace_with_aws_secret_access_key
```
- Run these commands
```bash
cd _infra
pip install pipenv
pipenv install      # to install Ansible and awscli
pipenv run ansible-galaxy install -r requirements.yml  # to install the Ansible roles
. ./env.sh
pipenv run aws --region eu-central-1  cloudformation create-stack \
        --template-body file://cf-single-instance.yml \
        --stack-name decibel-task \
        --parameters ParameterKey=KeyName,ParameterValue=id_rsa_decibel
watch pipenv run aws --region eu-central-1  cloudformation describe-stacks --stack-name decibel-task \
        --query "Stacks[0].StackStatus" --output text
pipenv run ansible-playbook -i inventories/api/hosts  playbooks/api-node.yml \
           --user=ubuntu --private-key ~/id_rsa_decibel
```

### Known issues: 
- If you want to change the log level you need another deploy by change the environment variable `LOG_LEVEL
- Change the configuration file means to change the docker image. It should be stored in the Ansible repo 
  so will be necessary only run the playbook
- Introduce the testing in the Ansible playbook 


## Additional content

### Docker image efficiency 
[Dive](https://github.com/wagoodman/dive)
```bash
dive decibel-api:latest
# [Image Details]
 
# Total Image size: 1.2 GB
# Potential wasted space: 12 MB
# Image efficiency score: 99 % 
```

### Call the api with test data

```bash
# In a local environment
cd app
LOG_LEVEL=DEBUG FLASK_ENV=development pipenv run flask run
# Another tab start from the project root
curl -X POST  -v -H "Content-Type: application/json" -d @test/testdata/data_1.json \
http://127.0.0.1:5000/get_anomalous_data
```
On the remote instance 
```bash
# Run pipenev in the _infra folder
PUBLIC_IP=$(pipenv run aws --region eu-central-1  cloudformation describe-stacks --stack-name decibel-task \
--query "Stacks[0].Outputs[?OutputKey=='PublicIP'].OutputValue" --output text)
# Test the endpoint 
curl -X POST  -v -H "Content-Type: application/json" -d @../test/testdata/data_1.json \
http://$PUBLIC_IP/get_anomalous_data

ssh -i ~/id_rsa_decibel ubuntu@${PUBLIC_IP} # connect in SSH to the instance
```

### Delete the CloudFormation stack
```bash
pipenv run aws --region eu-central-1  cloudformation delete-stack --stack-name decibel-task
```